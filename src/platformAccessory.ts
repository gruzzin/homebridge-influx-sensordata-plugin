import { Service, PlatformAccessory, CharacteristicValue } from 'homebridge';

import { HomebridgeInfluxPlatform } from './platform';


/**
 * Platform Accessory
 * An instance of this class is created for each accessory your platform registers
 * Each accessory may expose multiple services of different service types.
 */
export class InfluxDHT22 {
  private tempService: Service;
  private humService: Service;

  constructor(
    private readonly platform: HomebridgeInfluxPlatform,
    private readonly accessory: PlatformAccessory,
  ) {

    // set accessory information
    this.accessory.getService(this.platform.Service.AccessoryInformation)!
      .setCharacteristic(this.platform.Characteristic.Manufacturer, 'Espressiff')
      .setCharacteristic(this.platform.Characteristic.Model, 'ESP8266 Custom module')
      .setCharacteristic(this.platform.Characteristic.SerialNumber, accessory.context.device.serialNo.toString());

    // get the LightBulb service if it exists, otherwise create a new LightBulb service
    // you can create multiple services for each accessory
    this.tempService = (
      this.accessory.getService(this.platform.Service.TemperatureSensor) ||
      this.accessory.addService(this.platform.Service.TemperatureSensor)
    );
    this.tempService.setCharacteristic(this.platform.Characteristic.Name, accessory.context.device.location + ' room temperature');
    this.tempService.getCharacteristic(this.platform.Characteristic.CurrentTemperature)
      .onGet(this.handleCurrentTemperatureGet.bind(this));
    this.tempService.getCharacteristic(this.platform.Characteristic.StatusLowBattery)
      .onGet(this.handleBattStatusGet.bind(this));

    this.humService = (
      this.accessory.getService(this.platform.Service.HumiditySensor) ||
      this.accessory.addService(this.platform.Service.HumiditySensor)
    );
    this.humService.setCharacteristic(this.platform.Characteristic.Name, accessory.context.device.location + ' relative humidity');
    this.humService.getCharacteristic(this.platform.Characteristic.CurrentRelativeHumidity)
      .onGet(this.handleCurrentHumidityGet.bind(this));
  }

  async handleCurrentTemperatureGet(): Promise<CharacteristicValue> {
    this.platform.log.debug('Triggered GET CurrentTemperature');

    let currentValue = NaN;

    const queryApi = this.platform.influxClient.getQueryApi(this.platform.config.influxOrg);
    const fluxQuery = `from(bucket: "meteo-data")
                      |> range(start: -20m)
                      |> filter(fn: (r) => r["_measurement"] == "sensor-data")
                      |> filter(fn: (r) => r["_field"] == "temperature")
                      |> filter(fn: (r) => r["location"] == "${this.accessory.context.device.location}")
                      |> last()`;
    return await new Promise((resolve, reject) => {
      queryApi.queryRows(fluxQuery, {
        next(row, tableMeta) {
          const o = tableMeta.toObject(row);
          currentValue = o._value;
        },
        error: reject,
        complete() {
          resolve(currentValue);
        },
      });
    });
  }

  async handleBattStatusGet(): Promise<CharacteristicValue> {
    this.platform.log.debug('Triggered GET BattStatus');
    type InfluxQueryResult = {
      _value: number;
    };
    const queryApi = this.platform.influxClient.getQueryApi(this.platform.config.influxOrg);
    const fluxQuery = `from(bucket: "meteo-data")
                      |> range(start: -20m)
                      |> filter(fn: (r) => r["_measurement"] == "sensor-data")
                      |> filter(fn: (r) => r["_field"] == "voltage")
                      |> filter(fn: (r) => r["location"] == "${this.accessory.context.device.location}")
                      |> last()`;
    const data: InfluxQueryResult[] = await queryApi.collectRows(fluxQuery);
    const voltage = data[0]._value;
    if (voltage < 3.3 ) {
      return this.platform.Characteristic.StatusLowBattery.BATTERY_LEVEL_LOW;
    } else {
      return this.platform.Characteristic.StatusLowBattery.BATTERY_LEVEL_NORMAL;
    }
  }

  async handleCurrentHumidityGet(): Promise<CharacteristicValue> {
    this.platform.log.debug('Triggered GET CurrentHumidity');

    let currentValue = NaN;

    const queryApi = this.platform.influxClient.getQueryApi(this.platform.config.influxOrg);
    const fluxQuery = `from(bucket: "meteo-data")
                      |> range(start: -20m)
                      |> filter(fn: (r) => r["_measurement"] == "sensor-data")
                      |> filter(fn: (r) => r["_field"] == "humidity")
                      |> filter(fn: (r) => r["location"] == "${this.accessory.context.device.location}")
                      |> last()`;
    return await new Promise((resolve, reject) => {
      queryApi.queryRows(fluxQuery, {
        next(row, tableMeta) {
          const o = tableMeta.toObject(row);
          currentValue = o._value;
        },
        error: reject,
        complete() {
          resolve(currentValue);
        },
      });
    });
  }
}
